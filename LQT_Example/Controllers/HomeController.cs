﻿using DHL_ILOGIC_SAMPLEWEBSITE.App_Start;
using DHL_ILOGIC_SAMPLEWEBSITE.Models;
using EventSource4Net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Controllers
{
    public class HomeController : Controller
    {
      
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Leads()
        {
            ViewBag.Message = "Setting page.";

            return View();
        }

   
        public JsonResult Test(CustomOrder dataneworder)
        {
           
             return Json(new { error404 = "NOT FOUND", Message = "Error Message" });
        }
       

        public JsonResult GetListCitys()
        {
            using (var client = new WebClient())
            {
                client.Encoding = UTF8Encoding.UTF8;
                var json = client.DownloadString("https://thongtindoanhnghiep.co/api/city");

                JToken jsonc = JToken.Parse(json);
                if (jsonc["LtsItem"] != null) return Json(jsonc["LtsItem"].ToString());
            }
            return Json(new { error404 = "NOT FOUND", Message = "Error Message." });
        }
        public JsonResult GetDistrict(string Dis)
        {
            using (var client = new WebClient())
            {
                client.Encoding = UTF8Encoding.UTF8;
                var json = client.DownloadString($"https://thongtindoanhnghiep.co/api/city/{Dis}/district");
                JToken jsonc = JToken.Parse(json);
                if (jsonc!= null) return Json(jsonc.ToString());
            }
            return Json(new { error404 = "NOT FOUND", Message = "Error Message." });
        }
        public JsonResult GetMainWork()
        {
            using (var client = new WebClient())
            {
                client.Encoding = UTF8Encoding.UTF8;
                var json = client.DownloadString("https://thongtindoanhnghiep.co/api/industry");

                JToken jsonc = JToken.Parse(json);
                if (jsonc["LtsItem"] != null) return Json(jsonc["LtsItem"].ToString());
            }
            return Json(new { error404 = "NOT FOUND", Message = "Error Message." });
        }
    
        public JsonResult GetListQuery()
        {

            List<Queryv2> t = new List<Queryv2>();
            DBConnect i = new DBConnect();
            t = i.ListQuery();
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CreateQuery(Query submitform)
        {
            
            if (submitform.search_cities != null && submitform.search_cities != "") submitform.search_cities = "&l=" + submitform.search_cities;
            else submitform.search_cities = "";
            if (submitform.search_keyword != null && submitform.search_keyword != "") submitform.search_keyword = "k=" + submitform.search_keyword.Replace(" ", "+");
            else submitform.search_keyword = "k=";
            if (submitform.main_job != null && submitform.main_job != "") submitform.main_job = "&i=" + submitform.main_job;
            else submitform.main_job = "";
            if (submitform.search_type == 1 && submitform.time == null) submitform.time = DateTime.Now.ToString("HH:mm");
            if (submitform.search_type == 1)
            {
                if (submitform.loop_type == 1 && (submitform.day_week == null || submitform.day_week == "")) submitform.day_week = System.DateTime.Now.ToString("dddd");;
            }
            try
            {
                if (submitform.ID_query == null) submitform.ID_query = "";
                submitform.ID_query= DateTime.Now.ToString("yyyy/dd/MM(HH:mm)|" + Path.GetRandomFileName().Replace("|", "").Replace("'", "").Replace("+", "").Replace(":", "")) +"|"+ submitform.ID_query ;
                DBConnect t = new DBConnect();
                t.AddnewQuery(submitform);
            }
            catch (Exception) { return Json(new { error404 = "NOT FOUND", Message = "Error Message" }); }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult UpdateQuery(Query submitform)
        {

            if (submitform.search_cities != null && submitform.search_cities != "") submitform.search_cities = "&l=" + submitform.search_cities;
            else submitform.search_cities = "";
            if (submitform.search_keyword != null && submitform.search_keyword != "") submitform.search_keyword = "k=" + submitform.search_keyword.Replace(" ", "+");
            else submitform.search_keyword = "k=";
            if (submitform.main_job != null && submitform.main_job != "") submitform.main_job = "&i=" + submitform.main_job;
            else submitform.main_job = "";
            if (submitform.search_type == 1 && submitform.time == null) submitform.time = DateTime.Now.ToString("HH:mm");
            if (submitform.search_type == 1)
            {
                if (submitform.loop_type == 1 && (submitform.day_week == null || submitform.day_week == "")) submitform.day_week = System.DateTime.Now.ToString("dddd"); ;
            }
            try
            {
                DBConnect t = new DBConnect();
                t.RemoveQuery(submitform.ID_query);
                t.AddnewQuery(submitform);
            }
            catch (Exception) { return Json(new { error404 = "NOT FOUND", Message = "Error Message" }); }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult GetDetail(string QueryID)
        {
            System.Diagnostics.Debug.WriteLine(QueryID);

            List<Companiesv2> t = new List<Companiesv2>();
            DBConnect i = new DBConnect();
            t = i.ListCompanies(QueryID);
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDetail_Lead(string LeadID)
        {

            List<Companiesv2> t = new List<Companiesv2>();
            DBConnect i = new DBConnect();
            t = i.ListCompanies_Lead(LeadID);
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_Contact_List(string ID_VAT)
        {

            List<ListContact> t = new List<ListContact>();
            DBConnect i = new DBConnect();
            t = i.GetContactList(ID_VAT);
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        async Task<string> ProcessUrlAsync(string url)
        {try
            {
                using (var webClient = new WebClient())
                {
                    string data = await webClient.DownloadStringTaskAsync(new Uri(url));
                    System.Diagnostics.Debug.WriteLine(data);
                    return data;
                }
            } catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); return ""; }
        }

        public JsonResult GetListPlan()
        {
            List<LeadsPlan> t = new List<LeadsPlan>();
            DBConnect i = new DBConnect();
            t = i.ListPlans();
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteListPlan(string ID_leads)
        {
            DBConnect i = new DBConnect();
            i.DeletePlans(ID_leads);
            return Json(new { OK = "done", Message = "Error Message" });
        }
        public JsonResult DeleteListQuery(string Id_Query)
        {
            DBConnect i = new DBConnect();
            i.DeleteQuery(Id_Query);
            return Json(new { OK = "done", Message = "Error Message" });
        }
        public JsonResult CreatePlans(LeadsPlan submitform)
        {
            try
            {
                submitform.ID_lead = DateTime.Now.ToString("yyyy/dd/MM(HH:00)") +"|"+ Path.GetRandomFileName().Replace("|", "")+"|"+ DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                System.Diagnostics.Debug.WriteLine("ahiihhi " + DateTime.Now.ToString("yyyy/dd/MM(HH:00)") + "|" + Path.GetRandomFileName().Replace("|", "") + "|" + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                DBConnect t = new DBConnect();
                submitform.Completion_date = DateTime.Parse(submitform.Completion_date).ToString("dd/MM/yyyy");
                t.AddnewPlans(submitform);
            }
            catch (Exception) { }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult UpdateComPanies(DetailCompanies submitform)
        {
            try
            {
                if (submitform != null)
                {
                    DBConnect t = new DBConnect();
                    //   t.AddnewPlans(submitform);
                    // Change status each lead plan
                    t.ChangeCompanies(submitform);
                    t.ChangeEachPlanStatus(submitform.StatusLead, submitform.IDLead, submitform.VAT);
                }
            }
            catch (Exception) { }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult UpdateComPanies_Querry(DetailCompanies submitform)
        {
            try
            {
                if (submitform != null)
                {
                    DBConnect t = new DBConnect();
                    //   t.AddnewPlans(submitform);
                    // Change status each lead plan
                    t.ChangeCompanies(submitform);
                }
            }
            catch (Exception) { }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult EditOldPlans(LeadsPlan submitform)
        {
            try
            {
              //  submitform.ID_lead = DateTime.Now.ToString("yyyy/dd/MM(HH:00)|" + Path.GetRandomFileName().Replace("|", ""));
                DBConnect t = new DBConnect();
                t.UpdatePlans(submitform);
            }
            catch (Exception) { }
            return Json(new { done = "Success", Message = "Success Message" });
        }
        public JsonResult AddCompaniesToPlan(List<String> listID,string ID_plan)
        {
            DBConnect t = new DBConnect();
            t.AddCompaniesToPlans(listID, ID_plan);
            return Json(new { done = "Success", Message = "Success Message" });
        }


        public JsonResult Add_Contact_List(string listcontact,string id_vat)
        {
            System.Diagnostics.Debug.WriteLine(" asd sad asd asd ad :" + id_vat);
            DBConnect t = new DBConnect();
            if (id_vat != null && id_vat != "")
            {
                t.DeteleAllContact(id_vat);
                if (listcontact != null && listcontact != "")
                {
                    JToken jsonc = JToken.Parse(listcontact);
                    System.Diagnostics.Debug.WriteLine(jsonc);
                    foreach(JToken item in jsonc)
                    {
                        string name = (item["name"]!=null?item["name"].ToString():"");
                        string role = (item["role"] != null ? item["role"].ToString() : "");
                        string phone = (item["phone"] != null ? item["phone"].ToString() : "");
                        string email = (item["email"] != null ? item["email"].ToString() : "");
                        t.AddNewContact(name, role, phone, email, id_vat);
                    }
                }
            }
            return Json(new { done = "Success", Message = "Success Message" });
        }


        public JsonResult DeleteCompaniesFromPlan(List<String> listID, string ID_plan)
        {
            System.Diagnostics.Debug.WriteLine(listID.Count());
            System.Diagnostics.Debug.WriteLine(ID_plan);
            DBConnect t = new DBConnect();
            foreach(string id in listID)
            {
                t.deletePlans_Companies(id, ID_plan);
            }
            return Json(new { done = "Success", Message = "Success Message" });
        }
    }
}