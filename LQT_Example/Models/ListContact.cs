﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class ListContact
    {
        public ListContact(string name, string role, string phone, string email)
        {
            this.name = name;
            this.role = role;
            this.phone = phone;
            this.email = email;
        }

        public string name { get; set; }
        public string role { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}