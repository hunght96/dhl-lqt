﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class Query
    {
        public string ID_query { get; set; }
        public string search_keyword { get; set; }
        public int age_range { get; set; }
        public int loop_type { get; set; }
        public string main_job { get; set; }
        public string search_cities { get; set; }
        public int search_type { get; set; }
        public string time { get; set; }
        public int type_companies { get; set; }
        public string day_week { get; set; }
    }
}