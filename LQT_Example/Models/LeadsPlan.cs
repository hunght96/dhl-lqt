﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class LeadsPlan
    {
        public string ID_lead { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Completion_date { get; set; }
        public int Total_number { get; set; }
        public string status { get; set; }
    }
}