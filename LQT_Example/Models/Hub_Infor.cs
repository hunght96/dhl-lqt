﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class Hub_Infor
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string Level1 { get; set; }

    }
}