﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class QueryTable
    {
        public string ID_query { get; set; }
        public string Status { get; set; }
        public int Success { get; set; }
        public int Fail { get; set; }
        public int loop_type { get; set; }
        public string Last_time { get; set; }
    }
}