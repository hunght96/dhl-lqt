﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class CustomOrder
    {

        public string trackingcode { get; set; }
        public string waybillno { get; set; }
        public string parcelsize { get; set; }
        public string parcelcate { get; set; }
        public string consigneename { get; set; }
        public string consigneephone { get; set; }
        public string partnername { get; set; }
        public string partnephone { get; set; }
        public string pickuphubcode { get; set; }
        public string dropoffhubcode { get; set; }
        public string remark { get; set; }
        public string orderBooking { get; set; }
        public string Status { get; set; }
        public string serviceId { get; set; }
    }
}