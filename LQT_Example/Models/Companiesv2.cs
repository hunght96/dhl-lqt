﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class Companiesv2
    {
        public Companiesv2(string vAT_number, string public_name_vn, string public_name_en, string address_company, string date_public, string author_company, string main_work, string date_vat, string close_date, string trade_name, string phone, string fax, string create_date, string iD_query, string contactRole, string email, string statusComment, string description, string dateevent, string notes, string next, string status)
        {
            VAT_number = vAT_number;
            Public_name_vn = public_name_vn;
            Public_name_en = public_name_en;
            Address_company = address_company;
            Date_public = date_public;
            Author_company = author_company;
            Main_work = main_work;
            Date_vat = date_vat;
            Close_date = close_date;
            Trade_name = trade_name;
            Phone = phone;
            Fax = fax;
            Create_date = create_date;
            ID_query = iD_query;
            ContactRole = contactRole;
            Email = email;
            StatusComment = statusComment;
            Description = description;
            Dateevent = dateevent;
            Notes = notes;
            Next = next;
            Status = status;
        }

        public string VAT_number { get; set; }
        public string Public_name_vn { get; set; }
        public string Public_name_en { get; set; }
        public string Address_company { get; set; }
        public string Date_public { get; set; }
        public string Author_company { get; set; }
        public string Main_work { get; set; }
        public string Date_vat { get; set; }
        public string Close_date { get; set; }
        public string Trade_name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Create_date { get; set; }
        public string ID_query { get; set; }
        public string ContactRole { get; set; }
        public string Email { get; set; }
        public string StatusComment { get; set; }
        public string Description { get; set; }
        public string Dateevent { get; set; }
        public string Notes { get; set; }
        public string Next { get; set; }
        public string Status { get; set; }
    }
}