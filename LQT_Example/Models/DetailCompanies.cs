﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.Models
{
    public class DetailCompanies
    {
        public string VAT { get; set; }
        public string CompaniesName { get; set; }
        public string CompaniesAge { get; set; }
       
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
      
        public string StatusLead { get; set; }
        public string IDLead { get; set; }
        public string ContactRole { get; set; }
        public string Email { get; set; }
        public string StatusComment { get; set; }
        public string Description { get; set; }
        public string Dateevent { get; set; }
        public string Notes { get; set; }
        public string Next { get; set; }


    }
}