﻿using DHL_ILOGIC_SAMPLEWEBSITE.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace DHL_ILOGIC_SAMPLEWEBSITE.App_Start
{
    public class DBConnect
    {
        String connetionString = @"Data Source=199.40.64.76;Initial Catalog=Test_ILG;User ID=sa;Password=H0w@r3y0u";

        public void AddnewQuery(Query t)
        {
            try
            {
             
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Insert into Query_List values(N'"
                    + t.ID_query + "',N'"
                    + t.search_keyword + "','"
                    + t.age_range + "','"
                    + t.loop_type + "',N'"
                    + t.main_job + "',N'"
                    + t.search_cities + "','"
                    + t.search_type + "','"
                    + t.time + "',N'" +
                    t.type_companies + "','" +
                    t.day_week + "','','" +
                    "Wait" + "','" +
                    "0" + "','" +
                    "0"+ "','"+DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+"')";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public void AddnewPlans(LeadsPlan t)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Insert into Lead_Plan values(N'"
                    + t.ID_lead + "',N'"
                    + t.Name + "',N'"
                    + t.Description + "','"
                    + t.Completion_date + "','"
                    +"0" + "',N'"
                    +t.status+"')";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public void AddCompaniesToPlans(List<String> list, string Id_lead)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                foreach (string t in list)
                {
                    deletePlans_Companies(t, Id_lead);
                    string querry = "Insert into Lead_Companies values('"
                       + Id_lead + "','"
                       + t + "',N'New')";
                    command = new SqlCommand(querry, cnn);
                    adapter.InsertCommand = command;
                    adapter.InsertCommand.ExecuteNonQuery();

                }
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void UpdatePlans(LeadsPlan t)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Update Lead_Plan set Name=N'"+t.Name+"',Description=N'"+t.Description+"',Completion_date=N'"+t.Completion_date+ "',Status=N'"+t.status+"' where ID_Lead='" + t.ID_lead+"'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public void ChangeCompanies(DetailCompanies t)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Update Companies set "+
                                "Contact_Role=N'"+t.ContactRole+"',"+
                                "ChuSoHuu=N'"+t.ContactName+"',"+
                                "Phone=N'"+t.ContactPhone+"',"+
                                "Email=N'"+t.Email+"',"+
                                "Status_Comment=N'"+t.StatusComment+"',"+
                                "Des=N'"+t.Description+"',"+
                                "Date_Event=N'"+t.Dateevent+"',"+
                                "Notes=N'"+t.Notes+"',"+
                                "Nextmove=N'"+t.Next+"' " +
                                "where MaSoThue='" + t.VAT + "'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public void ChangeEachPlanStatus(string status, string lead_id,string vat_id)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Update Lead_Companies set Companies_Status=N'" + status + "' where ID_Lead='"+ lead_id + "' AND MaSoThue='"+vat_id+"'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public List<Queryv2> ListQuery()
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select *,(select count(*) from Query_List as a,Query_Companies as b where a.ID_Query=b.ID_Query AND c.ID_Query=a.ID_Query group by a.ID_Query) from Query_List as c order by c.Date_Create desc";
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<Queryv2> newlist = new List<Queryv2>();
                while (dataReader.Read())
                {
                    Queryv2 i = new Queryv2();
                    i.ID_query = dataReader.GetValue(0).ToString();
                    i.search_keyword = dataReader.GetValue(1).ToString();
                    i.age_range = int.Parse(dataReader.GetValue(2).ToString());
                    i.loop_type = int.Parse(dataReader.GetValue(3).ToString());
                    i.main_job = dataReader.GetValue(4).ToString();
                    i.search_cities = dataReader.GetValue(5).ToString();
                    i.search_type = int.Parse(dataReader.GetValue(6).ToString());
                    i.time = dataReader.GetValue(7).ToString();
                    i.type_companies = int.Parse(dataReader.GetValue(8).ToString());
                    i.day_week = dataReader.GetValue(9).ToString();
                    i.Status = dataReader.GetValue(11).ToString();
                    i.Success = int.Parse((dataReader.GetValue(15) != null && dataReader.GetValue(15).ToString() != "") ? dataReader.GetValue(15).ToString() : "0");
                    i.Last_time = dataReader.GetValue(10).ToString().Split('(')[0];
                    i.Create_time= dataReader.GetValue(14).ToString();
                    System.Diagnostics.Debug.WriteLine(i.Last_time);
                    if (i.Last_time != null && i.Last_time != "") i.Last_time = DateTime.Parse(i.Last_time).ToString("dd/MM/yyyy HH:mm");
                    if (i.Create_time != null && i.Create_time != "") i.Create_time = DateTime.Parse(i.Create_time).ToString("dd/MM/yyyy HH:mm");
                    newlist.Add(i);
                }
                cnn.Close();
                return newlist;
            }
            catch (Exception) { return null; }
        }
        public List<LeadsPlan> ListPlans()
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select c.ID_Lead,c.Name,c.Description,c.Completion_date,(select count(*) from Lead_Companies as b,Lead_Plan as a where a.ID_Lead=b.ID_Lead AND a.ID_Lead=c.ID_Lead group by a.ID_Lead),c.Status  from Lead_Plan as c;";
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<LeadsPlan> newlist = new List<LeadsPlan>();
                while (dataReader.Read())
                {
                    LeadsPlan i = new LeadsPlan();
                    i.ID_lead = dataReader.GetValue(0).ToString();
                    i.Name = dataReader.GetValue(1).ToString();
                    i.Description = dataReader.GetValue(2).ToString();
                    i.Completion_date = dataReader.GetValue(3).ToString();
                    i.Total_number = int.Parse(dataReader.GetValue(4).ToString()!=""? dataReader.GetValue(4).ToString():"0");
                    i.status = dataReader.GetValue(5).ToString();
                    if (i.Total_number > 0)
                    {
                        string Status = checkstatus_plan(i.ID_lead);
                        System.Diagnostics.Debug.WriteLine(Status);
                        i.status = Status;
                        UpdatePlans(i);
                    }

                    newlist.Add(i);

                }
                cnn.Close();
                return newlist;
            }
            catch (Exception) { return null; }
        }
        public void DeletePlans(string ID)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "DELETE FROM Lead_Companies where Lead_Companies.ID_Lead='"+ ID + "';DELETE FROM Lead_Plan where Lead_Plan.ID_Lead='"+ ID + "';";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void RemoveQuery(string ID)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "DELETE FROM Query_List where Query_List.ID_Query='" + ID + "';";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void DeleteQuery(string ID)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "DELETE FROM Query_Companies where Query_Companies.ID_Query='" + ID + "';DELETE FROM Query_List where Query_List.ID_Query='" + ID + "';";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void deletePlans_Companies(string vat,string leadid)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "DELETE FROM Lead_Companies where ID_Lead='"+leadid+ "' AND MaSoThue='"+vat+"'";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void AddListCompanies(List<Companies> list)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                foreach (Companies t in list)
                {

                    string querry = "Insert into Companies values('"
                        + t.VAT_number + "',N'"
                        + t.Public_name_vn + "',N'"
                        + t.Public_name_en + "',N'"
                        + t.Address_company + "',N'"
                        + t.Date_public + "',N'"
                        + t.Author_company + "',N'"
                        + t.Main_work + "',N'" 
                        + t.Date_vat + "',N'" 
                        + t.Close_date + "',N'"
                        + t.Trade_name + "',N'"
                        + t.Phone + "',N'"
                        + t.Fax + "',N'"
                        + t.Create_date + "','','','','','','','')";
                    command = new SqlCommand(querry, cnn);
                    adapter.InsertCommand = command;
                    adapter.InsertCommand.ExecuteNonQuery();

                    querry = "Insert into Query_Companies values('"
                       + t.ID_query + "','"
                       + t.VAT_number + "')";
                    command = new SqlCommand(querry, cnn);
                    adapter.InsertCommand = command;
                    adapter.InsertCommand.ExecuteNonQuery();

                }
                cnn.Close();
            } catch (Exception) { }
        }

        public List<Companiesv2> ListCompanies(string ID_Query)
        {

            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select Query_Companies.ID_query,Companies.* from Companies,Query_Companies Where Query_Companies.ID_query='" + ID_Query + "' AND Query_Companies.MaSoThue=Companies.MaSoThue";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<Companiesv2> newlist = new List<Companiesv2>();
                while (dataReader.Read())
                {
                    string datec = dataReader.GetValue(8).ToString();
                    DateTime time_now = DateTime.Now;
                    DateTime tc = (datec != null && datec != "") ? DateTime.Parse(datec) : DateTime.Now;

                    newlist.Add(new Companiesv2(
                        dataReader.GetValue(1).ToString(),
                        dataReader.GetValue(2).ToString(),
                        dataReader.GetValue(3).ToString(),
                        dataReader.GetValue(4).ToString(),
                        (dataReader.GetValue(5).ToString() != "" ? DateTime.Parse(dataReader.GetValue(5).ToString()).ToString("dd/MM/yyyy") : dataReader.GetValue(5).ToString()),
                        dataReader.GetValue(6).ToString(),
                        dataReader.GetValue(7).ToString(),
                        (time_now.Year - tc.Year).ToString(),
                        (dataReader.GetValue(9).ToString() != "" ? DateTime.Parse(dataReader.GetValue(9).ToString()).ToString("dd/MM/yyyy") : dataReader.GetValue(9).ToString()),
                         dataReader.GetValue(10).ToString(),
                        dataReader.GetValue(11).ToString(),
                        dataReader.GetValue(12).ToString(),
                        dataReader.GetValue(13).ToString(),
                        dataReader.GetValue(0).ToString(),
                        dataReader.GetValue(14).ToString(),
                        dataReader.GetValue(15).ToString(),
                        dataReader.GetValue(16).ToString(),
                        dataReader.GetValue(17).ToString(),
                        dataReader.GetValue(18).ToString(),
                        dataReader.GetValue(19).ToString(),
                        dataReader.GetValue(20).ToString(),
                        ""
                    ));
                }
                cnn.Close();
               
                return newlist;
            }
            catch (Exception) { return null; }
        }
        public List<Companiesv2> ListCompanies_Lead(string LeadID)
        {

            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select Lead_Companies.ID_Lead,Companies.*,Lead_Companies.Companies_Status from Companies,Lead_Companies,Lead_Plan Where Lead_Companies.ID_Lead='" + LeadID + "' AND Lead_Companies.MaSoThue=Companies.MaSoThue AND Lead_Plan.ID_Lead=Lead_Companies.ID_Lead";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<Companiesv2> newlist = new List<Companiesv2>();
                while (dataReader.Read())
                {
                    string datec = dataReader.GetValue(8).ToString();
                    DateTime time_now = DateTime.Now;
                    DateTime tc = (datec != null && datec != "") ? DateTime.Parse(datec) : DateTime.Now;

                    newlist.Add(new Companiesv2(
                        dataReader.GetValue(1).ToString(),
                        dataReader.GetValue(2).ToString(),
                        dataReader.GetValue(3).ToString(),
                        dataReader.GetValue(8).ToString(),
                        (dataReader.GetValue(5).ToString() != "" ? DateTime.Parse(dataReader.GetValue(5).ToString()).ToString("dd/MM/yyyy") : dataReader.GetValue(5).ToString()),
                        dataReader.GetValue(6).ToString(),
                        dataReader.GetValue(7).ToString(),
                        (time_now.Year - tc.Year).ToString(),
                        (dataReader.GetValue(9).ToString() != "" ? DateTime.Parse(dataReader.GetValue(9).ToString()).ToString("dd/MM/yyyy") : dataReader.GetValue(9).ToString()),
                        dataReader.GetValue(10).ToString(),
                        dataReader.GetValue(11).ToString(),
                        dataReader.GetValue(12).ToString(),
                        dataReader.GetValue(13).ToString(),
                        dataReader.GetValue(0).ToString(),
                        dataReader.GetValue(14).ToString(),
                        dataReader.GetValue(15).ToString(),
                        dataReader.GetValue(16).ToString(),
                        dataReader.GetValue(17).ToString(),
                        dataReader.GetValue(18).ToString(),
                        dataReader.GetValue(19).ToString(),
                        dataReader.GetValue(20).ToString(),
                        dataReader.GetValue(21).ToString()
                    ));
                }
                cnn.Close();
                return newlist;
            }
            catch (Exception) { return null; }
        }

        public List<ListContact> GetContactList(string ID_VAT)
        {

            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select * From ListContact Where MaSoThue='"+ ID_VAT + "' ";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<ListContact> newlist = new List<ListContact>();
                while (dataReader.Read())
                {
                    newlist.Add(new ListContact(
                        dataReader.GetValue(1).ToString(),
                        dataReader.GetValue(2).ToString(),
                        dataReader.GetValue(3).ToString(),
                        dataReader.GetValue(4).ToString()
                    ));
                }
                cnn.Close();
                return newlist;
            }
            catch (Exception) { return null; }
        }
        public void DeteleAllContact(string ID_VAT)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "DELETE FROM ListContact Where MaSoThue='" + ID_VAT + "' ";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void AddNewContact(string name,string role,string phone,string email,string ID_VAT)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Insert into ListContact values('"
                    + ID_VAT + "',N'"
                    + name + "',N'"
                    + role + "','"
                    + phone + "','"
                    + email + "')";
                command = new SqlCommand(querry, cnn);
                adapter.InsertCommand = command;
                adapter.InsertCommand.ExecuteNonQuery();


                cnn.Close();
            }
            catch (Exception) { }
        }
        public string checkstatus_plan(string leadID)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select * from Lead_Companies where ID_Lead='" + leadID + "'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                int count = 0;
                int count_Unqualified = 0;
                int count_done = 0;
                int count_new = 0;
                while (dataReader.Read())
                {
                    string status = dataReader.GetValue(2).ToString();
                    if (status.CompareTo("Unqualified") == 0) count_Unqualified++;
                    if (status.CompareTo("Ordered") == 0) count_done++;
                    if (status.CompareTo("New") == 0) count_new++;
                    count++;
                }               
                cnn.Close();
                if (count == count_new) return "Not Started";
                else if (count_Unqualified == count && count != 0) return "Completed";
                else if (count_done == count && count != 0) return "Completed";
                else if (count != 0) return "In Progress";
            }
            catch (Exception) { return null; }
            return "Not Started";
        }
        
    }
}