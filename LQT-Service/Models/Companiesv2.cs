﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQT_Service.AppCode
{
    class Companiesv2
    {
        public Companiesv2()
        {

        }

        public string VAT_number { get; set; }
        public string Public_name_vn { get; set; }
        public string Public_name_en { get; set; }
        public string Address_company { get; set; }
        public string Date_public { get; set; }
        public string Author_company { get; set; }
        public string Main_work { get; set; }
        public string Date_vat { get; set; }
        public string Close_date { get; set; }
        public string Trade_name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Create_date { get; set; }
        public string ID_query { get; set; }
        public string ContactRole { get; set; }
        public string Email { get; set; }
        public string StatusComment { get; set; }
        public string Description { get; set; }
        public string Dateevent { get; set; }
        public string Notes { get; set; }
        public string Next { get; set; }
        public string Status { get; set; }
    }
}
