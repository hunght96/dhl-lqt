﻿using LQT_Service.AppCode;
using LQT_Service.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LQT_Service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        Timer Time_Query = new Timer();
        Task[] LQT_Query_List = new Task[7] { Task.Run(() => { }), Task.Run(() => { }), Task.Run(() => { }), Task.Run(() => { }), Task.Run(() => { }), Task.Run(() => { }), Task.Run(() => { }) };

        protected override void OnStart(string[] args)
        {

            Time_Query.Elapsed += new ElapsedEventHandler(Process_LQT_Query);
            Time_Query.Interval = 10000; //number in milisecinds  
            Time_Query.AutoReset = true;
            Time_Query.Start();

        }

        protected override void OnStop()
        {
        }

        public static void Write_Log(string Message)
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\LQT_LOG_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!File.Exists(filepath))
                {
                    // Create a file to write to.   
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void Process_LQT_Query(object source, ElapsedEventArgs e)
        {
            try
            {
                SQLConnect ab = new SQLConnect();
                Boolean check_task_run = false;
                List<Task> taskslist = new List<Task>();
                List<Query> t = ab.ListQuery();
                foreach (Query querys in t)
                {

                    check_task_run = false;

                    Boolean check_type = false;
                    if (querys.search_type == 1)
                    {
                        if (querys.loop_type == 0) if (querys.time.CompareTo(DateTime.Now.ToString("HH:mm")) == 0) check_type = true;
                            else if (querys.loop_type == 1)
                            {
                                if (querys.day_week.ToLower().IndexOf(System.DateTime.Now.ToString("dddd").ToLower()) != -1)
                                {
                                    if (querys.time.CompareTo(DateTime.Now.ToString("HH:mm")) == 0) check_type = true;
                                }
                            }
                    }
                    else check_type = true;
                    if (check_type == true)
                    {
                        for (int i = 0; i < 7; i++)
                            if (check_task_run == false)
                            {
                                Boolean checkrun = false;
                                Task New_Task;
                                if (LQT_Query_List[i].IsCompleted || LQT_Query_List[i].IsFaulted || LQT_Query_List[i].IsCanceled)
                                {

                                    New_Task = Task.Run(async () => { await Process_Query(querys); });
                                    LQT_Query_List[i] = New_Task;
                                    checkrun = true;
                                    Write_Log("[" + DateTime.Now + "][LQT][" + querys.ID_query + "] : Start Running");
                                }
                                if (checkrun == true)
                                {
                                    ab.SetRunning(querys.ID_query);
                                    check_task_run = true;
                                }

                            }
                    }
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }


        }
        private static async Task Process_Query(Query t)
        {
            SQLConnect abc = new SQLConnect();
            try
            {
                List<Companies> list_companys = new List<Companies>();
                DateTime time_now = DateTime.Now;
                int count = 0;
                using (var client = new WebClient())
                {
                    client.Encoding = UTF8Encoding.UTF8;
                    int totalpage = 0;
                    int totalrow = 0;
                    JToken json = JToken.Parse(client.DownloadString($"https://thongtindoanhnghiep.co/api/company?{t.search_keyword}{t.search_cities}&r=100{t.main_job}"));
                    if (json["Option"] != null) totalrow = json["Option"]["TotalRow"] != null ? (int)json["Option"]["TotalRow"] : 0;
                    if (totalrow > 0)
                    {
                        System.Diagnostics.Debug.WriteLine(totalrow);

                        Write_Log("[" + DateTime.Now + "][LQT][" + t.ID_query + "] : Total Rows: " + totalrow);
                        totalpage = (totalrow / 100) + 1;
                        int page = 1;
                        while (page <= totalpage)
                        {
                            try
                            {
                                JToken getcompanys = JToken.Parse(client.DownloadString($"https://thongtindoanhnghiep.co/api/company?{t.search_keyword}{t.search_cities}&r=100&p={page}{t.main_job}"));
                                if (getcompanys != null && getcompanys["LtsItems"] != null)
                                {
                                    foreach (JToken itemc in getcompanys["LtsItems"])
                                    {
                                        count++;
                                        Write_Log("[" + DateTime.Now + "][LQT][" + t.ID_query + "] : Process Companies : " + count + "/" + totalrow + " (" + itemc["MaSoThue"].ToString() + ")");
                                        DateTime Vat_create = (itemc["NgayCap"] != null && itemc["NgayCap"].ToString() != "") ? DateTime.Parse(itemc["NgayCap"].ToString()) : DateTime.Now;
                                        if (t.age_range > 0 && (time_now.Year - Vat_create.Year) <= t.age_range)
                                        {
                                            Boolean check_type_com = true;
                                            Companiesv2 check = abc.CheckExit(itemc["MaSoThue"].ToString());
                                            if (check != null && check.VAT_number != null)
                                            {
                                                if ((check.Close_date != null && check.Close_date != "") && t.type_companies == 0) check_type_com = false;
                                                if (check_type_com == true)
                                                {
                                                    abc.AddConnectKey(itemc["MaSoThue"].ToString(), t.ID_query);
                                                }
                                            }
                                            else
                                            {
                                                using (var clientx = new WebClient())
                                                {
                                                    clientx.Encoding = UTF8Encoding.UTF8;
                                                    JToken item = JToken.Parse(clientx.DownloadString($"https://thongtindoanhnghiep.co/api/company/{itemc["MaSoThue"]}"));

                                                    if ((item["NgayDongMST"] != null && item["NgayDongMST"].ToString() != "") && t.type_companies == 0) check_type_com = false;

                                                    if (check_type_com == true)
                                                    {
                                                        list_companys.Add(new Companies(
                                                        item["MaSoThue"].ToString().Replace("'", "''"),
                                                        item["Title"].ToString().Replace("'", "''"),
                                                        item["TitleEn"].ToString().Replace("'", "''"),
                                                        item["DiaChiCongTy"].ToString().Replace("'", "''"),
                                                        item["NgayBatDauHopDong"].ToString().Replace("'", "''"),
                                                        item["ChuSoHuu"].ToString().Replace("'", "''"),
                                                        item["NganhNgheTitle"].ToString().Replace("'", "''"),
                                                        item["NgayCap"].ToString().Replace("'", "''"),
                                                        item["NgayDongMST"] != null ? item["NgayDongMST"].ToString().Replace("'", "''") : "",
                                                        item["TitleEn"].ToString().Replace("'", "''"),
                                                        item["NoiDangKyQuanLy_DienThoai"].ToString().Replace("'", "''"),
                                                        item["NoiDangKyQuanLy_DienThoai"].ToString().Replace("'", "''"),
                                                        item["NamTaiChinh"].ToString().Replace("'", "''"),
                                                        t.ID_query
                                                        ));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    page++;
                                }
                                else page = totalpage + 1;
                            }
                            catch (Exception ex) { Write_Log("[" + DateTime.Now + "][LQT][" + t.ID_query + "] : Process Companies : " + ex.ToString() + " "); }
                        }

                        Write_Log("[" + DateTime.Now + "][LQT][" + t.ID_query + "] : Total Companies: " + list_companys.Count());
                        abc.AddListCompanies(list_companys);
                    }
                }


                abc.SetDone(t.ID_query);
                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                Write_Log("[" + DateTime.Now + "][LQT][" + t.ID_query + "] : Error: " + ex.ToString());
                abc.SetError(t.ID_query);
            }
        }
    }
}
