﻿using LQT_Service.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQT_Service.AppCode
{
    class SQLConnect
    {
        String connetionString = @"Data Source=199.40.64.76;Initial Catalog=Test_ILG;User ID=sa;Password=H0w@r3y0u";
        public List<Query> ListQuery()
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select * from Query_List as c where c.Type_status Like 'Wait' OR c.Search_type='1'";
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                List<Query> newlist = new List<Query>();
                while (dataReader.Read())
                {
                    Query i = new Query();
                    i.ID_query = dataReader.GetValue(0).ToString();
                    i.search_keyword = dataReader.GetValue(1).ToString();
                    i.age_range = int.Parse(dataReader.GetValue(2).ToString());
                    i.loop_type = int.Parse(dataReader.GetValue(3).ToString());
                    i.main_job = dataReader.GetValue(4).ToString();
                    i.search_cities = dataReader.GetValue(5).ToString();
                    i.search_type = int.Parse(dataReader.GetValue(6).ToString());
                    i.time = dataReader.GetValue(7).ToString();
                    i.type_companies = int.Parse(dataReader.GetValue(8).ToString());
                    i.day_week = dataReader.GetValue(9).ToString();
                    newlist.Add(i);
                }
                cnn.Close();
                return newlist;
            }
            catch (Exception) { return null; }
        }
        public void SetRunning(String ID_Query)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = $"Update Query_List set Type_status=N'Running' where ID_Query='{ID_Query}'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void SetDone(String ID_Query)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = "Update Query_List set Type_status=N'Done',Last_time=N'" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "' where ID_Query='" + ID_Query + "'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void SetError(String ID_Query)
        {
            try
            {

                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string querry = $"Update Query_List set Type_status=N'Error',Last_time=N'" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "' where ID_Query='" + ID_Query + "'";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception) { }
        }
        public void AddListCompanies(List<Companies> list)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();
                foreach (Companies t in list)
                {
                    try
                    {
                        string querry = "Insert into Companies values('"
                            + t.VAT_number + "',N'"
                            + t.Public_name_vn + "',N'"
                            + t.Public_name_en + "',N'"
                            + t.Address_company + "',N'"
                            + t.Date_public + "',N'"
                            + t.Author_company + "',N'"
                            + t.Main_work + "',N'"
                            + t.Date_vat + "',N'"
                            + t.Close_date + "',N'"
                            + t.Trade_name + "',N'"
                            + t.Phone + "',N'"
                            + t.Fax + "',N'"
                            + t.Create_date + "','','','','','','','')";
                        command = new SqlCommand(querry, cnn);
                        adapter.InsertCommand = command;
                        adapter.InsertCommand.ExecuteNonQuery();
                    }
                    catch (Exception) { }
                    try
                    {
                        string querry = "Insert into Query_Companies values('"
                           + t.ID_query + "','"
                           + t.VAT_number + "')";
                        command = new SqlCommand(querry, cnn);
                        adapter.InsertCommand = command;
                        adapter.InsertCommand.ExecuteNonQuery();
                    }
                    catch (Exception) { }

                }
                cnn.Close();
            }
            catch (Exception ex)
            {
                Write_Log("[" + DateTime.Now + "][LQT][] : Error: " + ex.ToString());
            }
        }

        public Companiesv2 CheckExit(string ID_Vat)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataReader dataReader;
                String querry = "Select Companies.MaSoThue,Companies.NgayCap,Companies.NgayDongMST from Companies where Companies.MaSoThue='" + ID_Vat + "';";
                System.Diagnostics.Debug.WriteLine(querry);
                command = new SqlCommand(querry, cnn);
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    string datec = dataReader.GetValue(8).ToString();
                    DateTime time_now = DateTime.Now;
                    DateTime tc = (datec != null && datec != "") ? DateTime.Parse(datec) : DateTime.Now;
                    Companiesv2 t = new Companiesv2();
                    t.VAT_number = dataReader.GetValue(0).ToString();
                    t.Date_vat = dataReader.GetValue(1).ToString();
                    t.Close_date = dataReader.GetValue(2).ToString();
                    return t;
                }
                cnn.Close();
                return null;
            }
            catch (Exception) { return null; }
        }
        public static void Write_Log(string Message)
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\LQT_LOG_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!File.Exists(filepath))
                {
                    // Create a file to write to.   
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void AddConnectKey(string ID_Vat, string ID_Query)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(connetionString);
                cnn.Open();
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();

                try
                {
                    string querry = "Insert into Query_Companies values('"
                       + ID_Query + "','"
                       + ID_Vat + "')";
                    command = new SqlCommand(querry, cnn);
                    adapter.InsertCommand = command;
                    adapter.InsertCommand.ExecuteNonQuery();
                }
                catch (Exception) { }

                cnn.Close();
            }
            catch (Exception ex)
            {
                Write_Log("[" + DateTime.Now + "][LQT][] : Error: " + ex.ToString());
            }
        }
    }
}
